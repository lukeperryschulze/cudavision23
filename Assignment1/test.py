'''MLP Mixer model code'''

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import yaml
import os
import pdb
import einops

# should be added into a utils file in the future
def set_seed():
    # Set a fixed random seed for reproducibility

    # Set the random seed for PyTorch
    torch.manual_seed(9001)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # If you are using CUDA, set the seed for CUDA as well

    if torch.cuda.is_available():
        torch.cuda.manual_seed(9001)

class MLPBlock(nn.Module):
    '''
    Parameters:
         dim: int
         Input and Output dimension of the entire block
         hidden_dim: int
         hidden layer dimension
    '''
    def __init__(self, dim, hidden_dim):
        super().__init__()
        self.linear_1 = nn.Linear(dim, hidden_dim)
        self.activation = nn.GELU()
        self.linear_2 = nn.Linear(hidden_dim, dim)

    def forward(self, x):
        '''

        :param x: torch.tensor
        Input tensor of shape (batch, n_channels, n_patches)
        :return: torch.tensor, which has the shape (batch, n_channels, n_patches)
        '''
        print(x.shape)
        x = self.linear_1(x)
        x = self.activation(x)
        x = self.linear_2(x)

        return x


class MixerBlock(nn.Module):
    '''Parameters:
           n_patches : int
           hidden_dim: int
           This was the C value in the paper, the dimension which we project out patches to
           tokens_mlp_dim : int
           Hidden dimension for doing token-mixing
           channels_mlp_dim: int
           Hidden dimension for doing channel mixing
   '''


    def __init__(self, *, n_patches, hidden_dim, token_mlp_dim, channels_mlp_dim):
        super().__init__()

        self.norm_1 = nn.LayerNorm(hidden_dim)
        self.norm_2 = nn.LayerNorm(hidden_dim)

        self.token_mlp_block = MLPBlock(n_patches, token_mlp_dim)
        self.channel_mlp_block = MLPBlock(hidden_dim, channels_mlp_dim)

    def forward(self, x):
        '''
        :param x: torch.Tensor
        It is of shape (batch_size, n_patches, hidden_dim)
        :return: torch. Tensor
        Shape: (batch_size, n_patches, hidden_dim)
        '''
        # Separate y, because we need x as a residual connection
        y = self.norm_1(x)  # (batch, n_patches, hidden_dim)
        # 3d pytorch inputs takes the last dim, i.e. the row, therefore we need to transpose
        y = y.permute(0, 2, 1)  # (batch_size, hidden_dim, n_patches)
        y = self.token_mlp_block(y)  # (batch_size, hidden_dim, n_patches)
        y = y.permute(0, 2, 1)  # (batch_size, n_patches, hidden_dim)
        x = x + y  # (batch_size, n_patches, hidden_dim)
        y = self.norm_2(x)
        res = x + self.channel_mlp_block(y)  # (n_samples, n_patches, hidden_dim)

        return res




class MLPMixer(nn.Module):
    """
    sticking all parts together

    Parameters:
        image_size: int (We assume squares due to us focussing on CIFAR)
            Height and width of the input image
        patch_size: int
            Height and width of the square patches
        tokens_mlp_dim: int
            Hidden Dim for token mixer MLP
        channels_mlp_dim: int
            Hidden dim for MLPBlock when doing the channel mixing
        n_classes : int
            Number of classes for classification
        hidden_dim : int
            dim of patch embeddings
        n_blocks : int
            number of mixerblocks in the architecture
    """

    def __init__(self, *, image_size, patch_size, tokens_mlp_dim, channels_mlp_dim, n_classes, hidden_dim, n_blocks):
        super().__init__()
        n_patches = (image_size // patch_size) ** 2  # assume divisibility

        self.patch_embedder = nn.Conv2d(3, hidden_dim, kernel_size=patch_size, stride=patch_size)

        self.blocks = nn.ModuleList([MixerBlock(
            n_patches=n_patches,
            hidden_dim = hidden_dim,
            token_mlp_dim=tokens_mlp_dim,
            channels_mlp_dim = channels_mlp_dim
        ) for _ in range(n_blocks)])

        self.pre_head_norm = nn.LayerNorm(hidden_dim)
        self.head_classifier = nn.Linear(hidden_dim, n_classes)

    def forward(self, x):
        '''Parameters:
        x : torch. Tensor
            Input batch of square images of shape (batch_size, n_channels, image_size, images_size)

        :returns torch. Tensor
            class logits of shape (n_samples, n_classes)
        '''

        x = self.patch_embedder(x) # (batch_size, hidden_dim, n_patches**0.5, n_patches**0.5)
        x = einops.rearrange(x, "n c h w -> n (h w) c") # (batch, n_patches, hidden_dim)


        for mixer_block in self.blocks:
            x = mixer_block(x)  # (batch, n_patches, hidden_dim)


        x = self.pre_head_norm(x)  # (batch_size, n_patches, hidden_dim)
        x = x.mean(dim=1)  # (batch, hidden_dim)
        y = self.head_classifier(x)  # (batch, n_classes)
        return y





if __name__ == "__main__":
    # I am a big fan of config files and I believe it is a good practise
    config = yaml.safe_load(open("config.yaml"))
    print(f"config: {config}")
    batch_size = int(config["BATCH_SIZE"])


    # may differ depending on the problem
    transform = transforms.Compose(
        [transforms.ToTensor(),
         ]) # put more transforms options here

    #load datasets (in the future it's possible that the whole process will be in a
    #separate file)
    train_dataset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                            download=True, transform=transform)
    test_dataset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=True, transform=transform)
    #create dataloaders
    trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size,
                                              shuffle=True, num_workers=2)
    testloader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size,
                                             shuffle=False, num_workers=2)


    '''Following is for testing purposes'''
    set_seed()

    #(batch size, values)
    example_images, _ = next(iter(trainloader))

    model = MLPMixer(image_size=32, patch_size=4, tokens_mlp_dim=2048, channels_mlp_dim=256,
                     n_classes=10, hidden_dim = 512, n_blocks=8)

    model(example_images)